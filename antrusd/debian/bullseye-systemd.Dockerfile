FROM debian:bullseye-slim

ENV  container docker
ENV LC_ALL C.UTF-8

RUN  apt update && \
     apt -y full-upgrade && \
     apt -y install systemd readline-common && \
     rm -f /etc/localtime && \
     ln -s /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
     rm -fr /var/cache/apt/archives/* /var/lib/apt/lists/* /var/log/apt/* /var/log/dpkg.log \
            /etc/systemd/system/*.wants/* \
            /lib/systemd/system/multi-user.target.wants/* \
            /lib/systemd/system/local-fs.target.wants/* \
            /lib/systemd/system/sockets.target.wants/*udev* \
            /lib/systemd/system/sockets.target.wants/*initctl* \
            /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
            /lib/systemd/system/systemd-update-utmp*

CMD ["/lib/systemd/systemd"]
