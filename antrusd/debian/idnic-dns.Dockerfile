FROM antrusd/debian:bullseye-systemd

RUN  apt update && \
     apt -y full-upgrade && \
     apt -y install busybox bind9 bind9-dnsutils openssh-server curl traceroute tcpdump iputils-arping iputils-ping iputils-tracepath whois && \
     rm -fr /var/cache/apt/archives/* /var/lib/apt/lists/* /var/log/apt/* /var/log/dpkg.log \
            /etc/systemd/system/*.wants/* \
            /lib/systemd/system/multi-user.target.wants/* \
            /lib/systemd/system/local-fs.target.wants/* \
            /lib/systemd/system/sockets.target.wants/*udev* \
            /lib/systemd/system/sockets.target.wants/*initctl* \
            /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
            /lib/systemd/system/systemd-update-utmp*
