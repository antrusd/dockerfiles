FROM mikefarah/yq:4 as yq

FROM debian:stable-slim
COPY --from=yq /usr/bin/yq /usr/bin/yq

RUN apt -y update; \
    apt -y full-upgrade; \
    apt -y install --no-install-recommends curl ca-certificates git gpg gpg-agent reprepro openssh-client; \
    apt -y autoclean; \
    apt -y autoremove; \
    adduser --shell /bin/bash --uid 1000 --gecos '' --home /builds --disabled-password repos; \
    rm -fr /var/lib/apt/* /var/log/* /tmp/root

USER repos:repos
