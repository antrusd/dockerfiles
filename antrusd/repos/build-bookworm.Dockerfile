FROM mikefarah/yq:4 as yq

FROM debian:bookworm-slim
COPY --from=yq /usr/bin/yq /usr/bin/yq

ENV DEBIAN_FRONTEND=noninteractive

RUN apt -y update; \
    apt -y full-upgrade; \
    apt -y install --no-install-recommends build-essential fakeroot sudo curl ca-certificates gpg gpg-agent; \
    adduser --shell /bin/bash --uid 1000 --gecos '' --home /builds --disabled-password repos; \
    echo "repos ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/repos; \
    chmod 0440 /etc/sudoers.d/repos; \
    apt -y autoclean; \
    apt -y autoremove; \
    rm -fr /var/lib/apt/* /var/log/*

USER repos:repos
