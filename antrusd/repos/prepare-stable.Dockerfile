FROM mikefarah/yq:4 as yq

FROM debian:stable-slim
COPY --from=yq /usr/bin/yq /usr/bin/yq

RUN apt -y update; \
    apt -y full-upgrade; \
    apt -y install --no-install-recommends curl ca-certificates make libmoo-perl libdpkg-perl libdebhelper-perl libfile-stripnondeterminism-perl; \
    mkdir -p /tmp/root; \
    for pkg in equivs devscripts dpkg-dev debhelper dh-strip-nondeterminism dh-autoreconf; do \
        apt download ${pkg}; \
        find * -name ${pkg}_*.deb -exec dpkg -x {} /tmp/root \; ; \
        rm -f ${pkg}_*.deb; \
    done; \
    cp -a /tmp/root/usr/bin/equivs-* /usr/bin/; \
    cp -a /tmp/root/usr/bin/mk-build-deps /usr/bin/; \
    cp -a /tmp/root/usr/bin/dpkg-* /usr/bin/; \
    cp -a /tmp/root/usr/bin/dh* /usr/bin/; \
    cp -a /tmp/root/usr/share/equivs /usr/share/; \
    cp -a /tmp/root/usr/share/perl5 /usr/share/; \
    apt -y autoclean; \
    apt -y autoremove; \
    adduser --shell /bin/bash --uid 1000 --gecos '' --home /builds --disabled-password repos; \
    rm -fr /var/lib/apt/* /var/log/* /tmp/root

USER repos:repos
