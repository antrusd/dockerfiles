FROM python:3.9-alpine

RUN  apk add --no-cache curl && \
     apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev && \
     pip install ansible==2.9.27 netaddr==0.8.0 && \
     apk del .build-deps gcc musl-dev libffi-dev && \
     rm -fr /root/.cache
