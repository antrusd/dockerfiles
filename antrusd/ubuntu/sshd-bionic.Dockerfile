FROM ubuntu:bionic

ENV container docker
ENV LC_ALL C.UTF-8

RUN apt -y autoclean && \
    apt -y update && \
    apt -y full-upgrade && \
    apt -y install systemd readline-common openssh-server sudo vim-tiny busybox && \
    cp /etc/vim/vimrc /etc/skel/.vimrc && \
    adduser --shell /bin/bash --uid 1000 --gecos '' --disabled-password ubuntu && \
    echo "ubuntu ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ubuntu && \
    chmod 0440 /etc/sudoers.d/ubuntu && \
    rm -fr \
        /var/cache/apt/archives/* \
        /var/lib/apt/lists/* \
        /var/log/apt/* \
        /var/log/dpkg.log \
        /etc/systemd/system/*.wants/* \
        /lib/systemd/system/multi-user.target.wants/* \
        /lib/systemd/system/local-fs.target.wants/* \
        /lib/systemd/system/sockets.target.wants/*udev* \
        /lib/systemd/system/sockets.target.wants/*initctl* \
        /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
        /lib/systemd/system/systemd-update-utmp*

CMD ["/lib/systemd/systemd"]
