FROM rancher/k3s:v1.22.3-k3s1

COPY calico.yaml /var/lib/rancher/k3s/server/manifests/calico.yaml
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD []
