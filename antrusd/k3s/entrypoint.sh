#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

if [ ${K3S_DC_API_SERVER:-x} == x ]; then
  set -- k3s server --flannel-backend=none --no-deploy=servicelb --no-deploy=traefik --bind-address=${HOSTNAME} --https-listen-port=443 --token=${K3S_DC_TOKEN:-"antrusd.k3s-calico"} --cluster-cidr=100.64.0.0/16 --kube-controller-manager-arg=node-cidr-mask-size=22 --kubelet-arg=max-pods=500 --kube-scheduler-arg=address=0.0.0.0 ${K3S_DC_SERVER_ARGS:-"--"}
else
  echo -n "wait for kube scheduler to up..."
  until wget -q --spider http://${K3S_DC_API_SERVER}:10251/healthz 2>/dev/null; do
    echo -n "."
    sleep 1
  done
  echo "done"

  set -- k3s agent --server=https://${K3S_DC_API_SERVER} --token=${K3S_DC_TOKEN:-"antrusd.k3s-calico"} ${K3S_DC_AGENT_ARGS:-"--"}
fi

exec "$@"
